#include <stdio.h>
#include <string.h>

void concat(char s1[], char s2[]) {
    strcat(s1, s2);
}
void removeStdchar(char array[]) {
    if (strchr (array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}
int main (){
    char s1[50];
    char s2[50];
    printf("Nhap chuoi thu nhat: \n");
    fgets(s1, sizeof(s1) * sizeof(char), stdin);
    removeStdchar(s1);
    s1[strlen(s1) -1] = ' ';

    printf("Nhap chuoi thu hai: \n");
    fgets(s2, sizeof(s2) * sizeof(char), stdin);
    removeStdchar(s2);

    concat(s1, s2);
    printf("Ket qua sau khi noi chuoi la: %s\n", s1);
    return 0;
}